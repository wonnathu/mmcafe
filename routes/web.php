<?php

use Illuminate\Support\Facades\Route;

//Route::redirect('/', 'login');
Route::redirect('/home', '/admin');

Route::get('/', 'WebController@index');

//Route::get('/home', 'HomeController@index')->name('home');

Auth::routes(['register' => false]);

Route::get('/admin', "HomeController@admin")->name('admin');

Route::resource('/table', "TableController");
Route::resource('/category', "CategoryController");
Route::resource('/menu', "MenuController");
Route::resource('/orderlist', "OrderController");

Route::post('/cart/add', 'CartController@add')->name('cart')->middleware('auth');

Route::get('/cart', 'CartController@index')->name('checkout')->middleware('auth');
Route::get('/cart/clear', 'CartController@clear')->name('clear')->middleware('auth');
Route::delete('/cart/destroy/{itemId}', 'CartController@destroy')->name('cart.destroy')->middleware('auth');
Route::patch('/cart/update/{itemId}', 'CartController@update')->name('cart.update')->middleware('auth');
Route::post('/order', 'CartController@order')->name('cart.order')->middleware('auth');

Route::get('/orderlist/status/{id}', "OrderController@status")->name('orderlist.status')->middleware('auth');

Route::get('/setting/{userId}', 'UserController@show')->name('user.setting');
