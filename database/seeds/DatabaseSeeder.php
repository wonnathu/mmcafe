<?php

use App\Category;
use App\Menu;
use App\Table;
use App\User;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);

        //User
        $user = new User();
        $user->name = 'Admin';
        $user->email = "admin@admin.com";
        $user->password = bcrypt('password');
        $user->role = 1;
        $user->save();

        //Waiter
        $waiter = new User();
        $waiter->name = "Waiter";
        $waiter->email = "waiter@waiter.com";
        $waiter->password = bcrypt('password');
        $waiter->role = 2;
        $waiter->save();

        //Kitchen
        $kitchen = new User();
        $kitchen->name = "Kitchen";
        $kitchen->email = "kitchen@kitchen.com";
        $kitchen->password = bcrypt('password');
        $kitchen->role = 3;
        $kitchen->save();

        //Table
        $tables = [1, 2, 3, 4, 5];
        foreach ($tables as $table) {
            $tb = new Table();
            $tb->table_name = $table;
            $tb->save();
        }

        //Category
        $categories = ['shan food', 'myanmar food', 'china food', 'Hot Drink', 'Cold Drink'];
        foreach ($categories as $category) {
            $ct = new Category();
            $ct->category_name = $category;
            $ct->save();
        }

        $menus = [
            [
                'Shan Noodle',
                '1000',
                'public/image/food.png',
                1,
            ],
            [
                'Moke Hin Gar',
                '1000',
                'public/image/food.png',
                2,
            ],
            [
                'Dim Sum',
                '2000',
                'public/image/food.png',
                3,
            ],
            [
                'Tea (normal)',
                '500',
                'public/image/food.png',
                4,
            ],
            [
                'Tea (special)',
                '1000',
                'public/image/food.png',
                4,
            ],
            [
                'Red Bull',
                '1000',
                'public/image/food.png',
                5,
            ],
            [
                'Shark',
                '1000',
                'public/image/food.png',
                5,
            ],
        ];

        foreach ($menus as $menu) {
            $mn = new Menu();
            $mn->menu_name = $menu[0];
            $mn->price = $menu[1];
            $mn->image = $menu[2];
            $mn->category_id = $menu[3];
            $mn->save();
        }

    }
}
