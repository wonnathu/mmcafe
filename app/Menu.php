<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Menu extends Model
{

    public function scopeFilterOn($query)
    {
        //dd(request('category_id'));
        if (request('category_id')) {
            return $query->where('category_id', request('category_id'));
        }
        // elseif(request('status') == 'trash'){
        //     $query->onlyTrashed();
        // }
    }

    public function category()
    {
        return $this->belongsTo('App\Category');
    }

    public function orders()
    {
        return $this->belongsToMany('App\Order', 'order_menu', 'menu_id', 'order_id');
    }

}
