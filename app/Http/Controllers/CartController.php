<?php

namespace App\Http\Controllers;

use App\Order;
use App\Table;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CartController extends Controller
{

    public function cart()
    {

    }

    public function add(Request $request)
    {
        //dd(auth()->id());
        \Cart::session(auth()->id())->add([
            'id' => $request->menu_id,
            'name' => $request->menu_name,
            'price' => $request->price,
            'quantity' => $request->qnt,
            'attributes' => array(),
        ]);

        return back();
    }

    public function index()
    {
        $cartItems = \Cart::session(auth()->id())->getContent();
        $tables = Table::orderBy('table_name')->get();
        return view('admin.cart.checkout', [
            'cartItems' => $cartItems,
            'tables' => $tables,
        ]);
    }

    public function clear()
    {
        \Cart::session(auth()->id())->clear();
        return redirect()->route('admin');
    }

    public function destroy($itemId)
    {
        \Cart::session(auth()->id())->remove($itemId);
        return back();
    }

    public function update($itemId, Request $request)
    {
        if(!empty($request->qnt)){
            \Cart::session(auth()->id())->update($itemId, array(
                'quantity' => array(
                    'relative' => false,
                    'value' => $request->qnt,
                ),
            ));
        }

        if(!empty($request->remark)){
            \Cart::session(auth()->id())->update($itemId, array(
                'attributes' => array(
                    'remark' => $request->remark,
                ),
            ));
        }
        


        return back();
    }

    public function order(Request $request)
    {

        $cartItems = \Cart::session(auth()->id())->getContent();

        if (count($cartItems) == 0) {
            return redirect()->route('admin');
        }

        $table = Table::find($request->table_id);

        $order = new Order();
        $order->table_id = $table->id;
        $order->table_name = $table->table_name;
        $order->save();
        
        //$order->menus()->attach($order->id, ['qnt' => 1]);
        foreach ($cartItems as $item) {
            //echo $item->attributes->remark. " ///";
            DB::table('order_menu')->insert(
                ['order_id' => $order->id, 'menu_id' => $item->id, 'qnt' => $item->quantity,'remark' => $item->attributes->remark, 'created_at' => now(), 'updated_at' => now()]
            );
        }
        //dd('end');
        return $this->clear();
    }
}
