<?php

namespace App\Http\Controllers;

use App\Category;
use App\Http\Controllers\Controller;
use App\Menu;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class MenuController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $menus = Menu::orderBy('id', 'desc')->paginate(9);
        return view('admin.menu.index', [
            'menus' => $menus,
            'no' => 1,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Category::orderBy('category_name')->get();
        //dd($categories);
        return view('admin.menu.create', [
            'categories' => $categories,
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'menu_name' => 'required',
            'price' => 'required',
            'category_id' => 'required',
        ]);

        $menu = new Menu();
        $menu->menu_name = $request->menu_name;
        $menu->price = $request->price;
        if ($request->image) {
            $path = $request->file('image')->store('public/image');
            $menu->image = $path;
        } else {
            $menu->image = "public/image/food.png";
        }
        $menu->category_id = $request->category_id;
        $menu->save();

        return redirect()->route('menu.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Menu  $menu
     * @return \Illuminate\Http\Response
     */
    public function show(Menu $menu)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Menu  $menu
     * @return \Illuminate\Http\Response
     */
    public function edit(Menu $menu)
    {
        $categories = Category::orderBy('category_name')->get();
        return view('admin.menu.edit', [
            'menu' => $menu,
            'categories' => $categories,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Menu  $menu
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Menu $menu)
    {
        $request->validate([
            'menu_name' => 'required',
            'price' => 'required',
            'category_id' => 'required',
        ]);

        $menu->menu_name = $request->menu_name;
        $menu->price = $request->price;
        if ($request->image) {
            if ($menu->image != "public/image/food.png") {
                Storage::delete($menu->image);
            }
            $path = $request->file('image')->store('public/image');
            $menu->image = $path;
        }
        $menu->category_id = $request->category_id;
        $menu->update();

        return redirect()->route('menu.index');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Menu  $menu
     * @return \Illuminate\Http\Response
     */
    public function destroy(Menu $menu)
    {
        if ($menu->image != "public/image/food.png") {
            Storage::delete($menu->image);
        }

        $menu->delete();
        return back();

    }
}
