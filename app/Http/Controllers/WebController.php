<?php

namespace App\Http\Controllers;

use App\Category;
use App\Menu;

class WebController extends Controller
{
    public function index()
    {
        $categories = Category::orderBy('category_name', 'asc')->get();
        $menus = Menu::orderBy('menu_name', 'asc')->get();

        return view('welcome', [
            'categories' => $categories,
            'menus' => $menus,
        ]);
    }
}
