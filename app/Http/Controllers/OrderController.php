<?php

namespace App\Http\Controllers;

use App\Order;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class OrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $orders = Order::filterOn()->orderBy('id', 'desc')->paginate(50);
        // $roles = Order::find(1)->menus()->get();
        // dd($roles);
        $total = 0;

        foreach ($orders as $order) {
            foreach ($order->menus()->get() as $ord) {
                $total += ($ord->price * $ord->pivot->qnt);
            }
        }

        //dd($total);

        return view('admin.order.index', [
            'orders' => $orders,
            'total' => $total,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function show(Order $order)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function edit(Order $order)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Order $order)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $order = Order::find(request('orderlist'));
        $orderlists = DB::table('order_menu')
            ->where('order_id', "$order->id")
            ->delete();

        $order->delete();
        return back();
    }

    public function status($id)
    {
        $order = Order::find($id);
        
        $status = $order->status;
        
        if($status == 0){
            $order->status = 1;
            $order->save();
        }
        else{
            $order->status = 0;
            $order->save();
        }

        return redirect()->route('orderlist.index');

    }
}
