<?php

namespace App\Http\Controllers;

use App\Category;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('home');
    }

    public function admin(Request $request)
    {
        $filter_category = Category::orderBy('category_name', 'asc')->get();
        $categories = Category::filterOn()->orderBy('category_name', 'asc')->get();

        $cartItems = \Cart::session(auth()->id())->getContent();
        $cart_count = count($cartItems);

        return view('admin.dashboard', [
            'filter_category' => $filter_category,
            'categories' => $categories,
            'cart_count' => $cart_count,
        ]);
    }
}
