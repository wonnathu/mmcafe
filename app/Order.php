<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    public function scopeFilterOn($query)
    {
        if (request('startdate') && request('enddate')) {
            return $query->whereDate('created_at', '>=', request('startdate'))->whereDate('created_at', '<=', request('enddate'));
        }
    }

    public function menus()
    {
        //return $this->belongsToMany('App\Menu', 'order_menu', 'order_id', 'menu_id');
        return $this->belongsToMany('App\Menu', 'order_menu', 'order_id', 'menu_id')->withPivot('order_id', 'menu_id', 'qnt' , 'remark');
        //return $this->belongsToMany('App\Menu')->withPivot('order_id', 'menu_id', 'qnt');
    }

    public function menus1()
    {
        //return $this->belongsToMany('App\Menu', 'order_menu', 'order_id', 'menu_id');
        return $this->belongsToMany('App\Menu', 'order_menu', 'order_id', 'menu_id')->withPivot('order_id', 'menu_id', 'qnt');
        //return $this->belongsToMany('App\Menu')->withPivot('order_id', 'menu_id', 'qnt');
    }
}
