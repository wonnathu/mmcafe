<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    public function menus()
    {
        return $this->hasMany('App\Menu');
    }

    public function scopeFilterOn($query)
    {
        if (request('category_id')) {
            return $query->where('id', request('category_id'));
        }
    }

}
