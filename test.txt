<form action="{{route('cart')}}" method="post">
@csrf
  <div class="card mb-3">
    <img src="{{ url('storage/'.str_replace('public/', '', $menu->image)) }}" class="card-img-top" alt="..." >
    <div class="card-body">
      <h5 class="card-title">{{$menu->menu_name}}</h5>
      <p class="card-text">Price: {{$menu->price}}</p>
      <div class="position-relative form-group">
        <label for="qnt" class="">Quantity</label>
        <input name="qnt" id="qnt" placeholder="0" type="number" min="1" class="form-control @error('qnt') border-danger @enderror" value="1">
        @error('qnt')
        <p class="text-danger mt-3">{{$message}}</p>
        @enderror
      </div>
      <input type="hidden" name="menu_id" value="{{$menu->id}}">
      <input type="hidden" name="menu_name" value="{{$menu->menu_name}}">
      <input type="hidden" name="price" value="{{$menu->price}}">
      <button type="submit" class="btn btn-primary">Add To Cart</button>
    </div>
  </div>
</form>