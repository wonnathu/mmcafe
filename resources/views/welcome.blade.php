<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>
<body style="background: white; position: relative;" data-spy="scroll" data-target=".navbar" data-offset="100">
    <div id="app">

        <a href="#" id="back-to-top" title="Back to top">
            <img src="{{asset('images/up-chevron-button.svg')}}" alt="Back to Top" style="width: 100%;">
        </a>

        <nav id="nav" class="navbar navbar-expand-lg fixed-top navbar-light bg-hide">
            <div class="container">
                <a class="navbar-brand cafe-title" href="#"><span class="h3">Romance Cafe</span></a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="navbar-nav mr-auto">
                        <li class="nav-item">
                            <a class="nav-link" href="#home">Home <span class="sr-only">(current)</span></a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#about">About</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#menu">Menu</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#chef">Chefs</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#feature">Feature</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#contact">Contact</a>
                        </li>
                    </ul>
                    <div class="my-2 my-lg-0">
                    @if (Route::has('login'))
                    <div class="top-right links">
                        @auth
                            <a href="{{ url('/admin') }}" class="btn cafe-search pl-5 pr-5">Dashboard</a>
                        @else
                            <a href="#" class="callnow">
                                Call Now!
                                <i class="fas fa-mobile-alt ml-2 mr-2"></i>
                                09797420445
                            </a>
                            <a href="{{ route('login') }}" class="btn cafe-search pl-5 pr-5">Login</a>
                        @endauth
                    </div>
                @endif
                    </div>
                </div>
            </div>
        </nav>

        <header id="home" class="carousel slide" data-ride="carousel"  >
            <ol class="carousel-indicators">
                <li data-target="#home" data-slide-to="0" class="active"></li>
                <li data-target="#home" data-slide-to="1"></li>
                <li data-target="#home" data-slide-to="2"></li>
            </ol>
            <div class="carousel-inner h-100" >
                <div class="carousel-item active">
                    <img src="{{asset('images/slider-image1.jpg')}}" class="d-block w-100" alt="..." >
                    <div class="carousel-caption d-none d-md-block">
                        <h5>Day Nourishing Retreat</h5>
                        <p>Spend the day with us and enjoy balanced healthy vegan meals, two yoga classes of your choice and two hours of pampering treatments from Bliss Beauty Bar. Exact timings depend on the day of the week.</p>
                    </div>
                </div>
                <div class="carousel-item">
                    <img src="{{asset('images/slider-image2.jpg')}}" class="d-block w-100" alt="..." >
                    <div class="carousel-caption d-none d-md-block">
                        <h5>Multi-day Retreat with meal plan</h5>
                        <p>Customize your own multi-day retreat schedule with a meal plan from Nourish, served before or after yoga class. Select from multiple options for breakfast, lunch and dinner.</p>
                    </div>
                </div>
                <div class="carousel-item">
                    <img src="{{asset('images/slider-image3.jpg')}}" class="d-block w-100" alt="..." >
                    <div class="carousel-caption d-none d-md-block">
                        <h5>Juice Cleanse with Jungle Detox</h5>
                        <p>To cleanse, hydrate and nourish your body, we partner with Jungle Detox Myanmar, a cold pressed raw juice company specializing in juice cleanse programs.</p>
                    </div>
                </div>
            </div>
            <a class="carousel-control-prev" href="#home" role="button" data-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
            </a>
            <a class="carousel-control-next" href="#home" role="button" data-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
            </a>
        </header>

        <main class="py-4">
                <div id="about" style="margin-bottom: 80px"></div>
                <div class="container" >
                    <div class="row">
                        <div class="col-md-6 p-5">
                            <span class="text-secondary">About Me</span>
                            <h1 class="cafe-title">Hello world</h1>
                            <p class="text-secondary">Lorem ipsum dolor sit amet consectetur, adipisicing elit. Maxime placeat veritatis eveniet est ducimus, dignissimos officiis, eum aliquid amet ea magni perferendis esse labore quae libero saepe dolorem sed quibusdam!</p>
                            <p class="text-secondary">Lorem, ipsum dolor sit amet consectetur adipisicing elit. Architecto possimus earum quos soluta neque. Vero porro, at nemo accusamus in voluptatum dicta nihil corrupti vel hic quo culpa quibusdam voluptates?</p>
                        </div>
                        <div class="col-md-6">
                            <img src="{{asset('images/about-image.jpg')}}" class="w-100" alt="">
                        </div>
                    </div>
                </div>

                <div id="menu" style="margin-bottom: 120px"></div>
                <div class="container mt-5 our-menu" >
                    <h2 class="cafe-title text-center">Our Menu</h2>
                    <hr class="website-hr">

                    <div class="row mt-5">
                        @foreach($menus as $menu)
                            <div class="col-md-6 ">
                                <div class="row ">
                                    <div class="col-md-3 ">
                                        <img src="{{ url('storage/'.str_replace('public/', '', $menu->image)) }}" alt="" class="w-100" >
                                    </div>
                                    <div class="col-md-9 ">
                                        <div class="row">
                                            <div class="col-7">
                                                <h2 class="menu-title">{{$menu->menu_name}}</h2>
                                                <span class="menu-category">{{$menu->category->category_name}}</span>
                                            </div>
                                            <div class="col-5">
                                                <h3 class="menu-price">{{$menu->price}} MMK</h3>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <hr style="margin: 20px 0">
                            </div>
                        @endforeach
                    </div>

                <!-- @foreach($categories as $category)
                    @if(count($category->menus))
                        <h2 class="h4 mt-5 website-title">{{$category->category_name}}</h2>

                        <div class="row mb-5">
                        @foreach($category->menus as $menu)
                            <div class="col-sm-6 col-md-4 col-xl-3 mb-3 mt-3">
                                <div class="card h-100 border-0">
                                    <img src="{{ url('storage/'.str_replace('public/', '', $menu->image)) }}" class="card-img-top" alt="..." style="width: 100%; height: 170px;">
                                    <div class="card-body pb-0" style="background: none;">
                                    <h5 class="card-title cafe-card-title">{{$menu->menu_name}}
                                        <small class="text-secondary">({{$category->category_name}})</small>
                                    </h5>
                                    <p class="card-text text-secondary"><i class="fas fa-comment-dollar"></i> {{$menu->price}} MMK</p>
                                    </div>
                                    <div class="card-footer pb-0">
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                    @endif
                @endforeach -->

                </div>

                <div id="chef" style="margin-bottom: 120px"></div>
                <div class="chef container">
                    <h2 class="cafe-title text-center">Meet our chefs</h2>
                    <hr class="website-hr">

                    <div class="row">
                    <div class="col-md-4">
                            <div class="bander">
                                <img src="{{asset('images/team-image1.jpg')}}" alt="Avatar" class="image">
                                <div class="overlay">
                                    <h2>Miss. Soda</h2>
                                    <p>Senior Chef</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="bander">
                                <img src="{{asset('images/team-image2.jpg')}}" alt="Avatar" class="image">
                                <div class="overlay">
                                    <h2>Miss. Jue</h2>
                                    <p>Senior Chef</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="bander">
                                <img src="{{asset('images/team-image3.jpg')}}" alt="Avatar" class="image">
                                <div class="overlay">
                                    <h2>Miss. Kay</h2>
                                    <p>Cake Specialist</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div id="feature"></div>
                <div class="website-feature">
                    <h1 class="cafe-title feature-title">WHY CHOOSE US?</h1>
                    <div class="container">
                        <div class="row">
                            <div class="col-xl-4 col-md-6 mb-4">
                                <div class="feature-thumb">
                                    <div class="feature-icon">
                                        <span><i class="fas fa-utensils"></i></span>
                                    </div>
                                    <h3 class="h4 text-center cafe-primary-color">SPECIAL DISH</h3>
                                    <p class="text-center text-secondary">Lorem ipsum dolor sit amet, consectetur adipiscing elitquisque tempus ac eget diam et laoreet phasellus.</p>
                                </div>
                            </div>
                            <div class="col-xl-4 col-md-6 mb-4">
                                <div class="feature-thumb">
                                    <div class="feature-icon">
                                        <span><i class="fas fa-coffee"></i></span>
                                    </div>
                                    <h3 class="h4 text-center cafe-primary-color">BLACK COFFEE</h3>
                                    <p class="text-center text-secondary">Lorem ipsum dolor sit amet, consectetur adipiscing elitquisque tempus ac eget diam et laoreet phasellus.</p>
                                </div>
                            </div>
                            <div class="col-xl-4 col-md-6 mb-4">
                                <div class="feature-thumb">
                                    <div class="feature-icon">
                                        <span><i class="fas fa-concierge-bell"></i></span>
                                    </div>
                                    <h3 class="h4 text-center cafe-primary-color">DINNER</h3>
                                    <p class="text-center text-secondary">Lorem ipsum dolor sit amet, consectetur adipiscing elitquisque tempus ac eget diam et laoreet phasellus.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div id="contact" style="margin-bottom: 120px"></div>
                <div class="container">
                    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3698.4808435983123!2d96.46805684308768!3d22.031194526229328!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x30cc9f7f6c97eb17%3A0xde970ec9d724597e!2sPyin%20Oo%20lwin!5e0!3m2!1sen!2smm!4v1588690252333!5m2!1sen!2smm" width="100%" height="450" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
                </div>

        </main>

        <footer>
            <div class="bg-dark">
                <div class="container p-5">
                    <div class="row">
                        <div class="col-md-4 text-center mt-5">
                            <h4 class="footer-title">RESERVATIONS</h4>
                            <hr class="website-hr">
                            <p class="footer-paragraph">Please call</p>
                            <p class="footer-paragraph">+959 797420445</p>
                        </div>
                        <div class="col-md-4 text-center mt-5">
                            <h4 class="footer-title">ADDRESS</h4>
                            <hr class="website-hr">
                            <p class="footer-paragraph">No(1500), Loving Road,</p>
                            <p class="footer-paragraph">Romance Country</p>
                        </div>
                        <div class="col-md-4 text-center mt-5">
                            <h4 class="footer-title">OPENING HOURS</h4>
                            <hr class="website-hr">
                            <p class="footer-paragraph">Mon-Thurs: 10:00 AM - 11:00 PM</p>
                            <p class="footer-paragraph">Fri-Sun: 11:00 AM - 02:00 AM</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="final-footer">
                <div class="container p-5">
                    <div class="row">
                        <div class="col-md-6 text-light mb-3">
                            © Copyright. All rights reserved | Designed by <a href="#" target="_blank" class="me">Me</a>
                        </div>
                        <div class="col-md-4 offset-md-2 mb-3">
                            <ul class="nav justify-content-center">
                                <li class="nav-item">
                                    <a class="nav-link active" href="#">
                                        <i class="fab fa-facebook h4 text-light"></i>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="#">
                                        <i class="fab fa-google-plus-g h4 text-light"></i>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="#">
                                        <i class="fab fa-twitter h4 text-light"></i>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </footer>
    </div>

    <script src="{{asset('js/app.js')}}"></script>
    <script src="https://cdn.jsdelivr.net/gh/cferdinandi/smooth-scroll/dist/smooth-scroll.polyfills.min.js"></script>
    <script>
        $(document).ready(function(){

            if ($('#back-to-top').length) {
                var scrollTrigger = 100, // px
                    backToTop = function () {
                        var scrollTop = $(window).scrollTop();
                        if (scrollTop > scrollTrigger) {
                            $('#back-to-top').addClass('show');
                            $('#nav').addClass('bg-show');
                            $('#nav').removeClass('bg-hide');
                            $('#nav').addClass('navbar-light');
                            $('#nav').removeClass('navbar-dark');
                        } else {
                            $('#back-to-top').removeClass('show');
                            $('#nav').addClass('bg-hide');
                            $('#nav').removeClass('bg-show');
                            $('#nav').addClass('navbar-dark');
                            $('#nav').removeClass('navbar-light');
                        }
                    };
                backToTop();
                $(window).on('scroll', function () {
                    backToTop();
                });
                $('#back-to-top').on('click', function (e) {
                    e.preventDefault();
                    $('html,body').animate({
                        scrollTop: 0
                    }, 700);
                });
            }
            var scroll = new SmoothScroll('a[href*="#"]');
        });

    </script>
</body>
</html>
