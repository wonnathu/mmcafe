@extends('admin.layout.master')

@section('title', "Dashboard")

@section('content')


<div class="clearfix" >
  <div class="float-left">
    <h1 class="cafe-title">Category List</h1>
  </div>
  <div class="float-right">
    <a href="{{route('category.create')}}" class="mb-2 mr-2 btn cafe-search mb-4">Add New category</a>
  </div>
</div>

  <div class="card ">
    <table class="mb-0 table table-striped">
      <thead>
      <tr class="cafe-category-table-header">
          <th class="pt-4 pb-4">#</th>
          <th class="pt-4 pb-4">Category Name</th>
          <th class="pt-4 pb-4">Action</th>
      </tr>
      </thead>
      <tbody>
      @foreach($categories as $category)
        <tr>
            <th scope="row">{{$no++}}</th>
            <td style="text-transform: capitalize;">{{$category->category_name}}</td>
            <td>

              <a href="{{route('category.edit', $category->id)}}" title="Edit" class="btn ">
                <i class="metismenu-icon pe-7s-note2 h5 text-success"></i>
              </a>


              <form action="{{ route('category.destroy', $category->id) }}" onsubmit="return confirm('{{ 'Are You Sure' }}');" method="POST" class="d-inline">
                @csrf
                @method('DELETE')
                <button type="submit" class="btn" title="Delete">
                  <i class="metismenu-icon pe-7s-trash h5 text-danger" ></i>
                </button>
            </form>

            </td>
        </tr>
      @endforeach
      </tbody>
    </table>

  </div>
@endsection
