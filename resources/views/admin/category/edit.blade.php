@extends('admin.layout.master')

@section('title', "Dashboard")

@section('content')
<div class="clearfix" >
  <div class="float-left">
    <h1 class="cafe-title">Edit Category</h1>
  </div>
  <div class="float-right">
    <a href="{{route('category.index')}}" class="mb-2 mr-2 btn cafe-search mb-4">Back</a>
  </div>
</div>

  <div class="main-card mb-3 card">
    <div class="card-body p-5">
        <form class="" method="post" action="{{route('category.update', $category->id)}}">
        @csrf
        @method('patch')
            <div class="position-relative form-group">
              <label for="name" class="">Category Name</label>
              <input name="category_name" id="name" placeholder="Enter the Category name" type="text" class="form-control @error('category_name') border-danger @enderror" value="{{$category->category_name}}">
              @error('category_name')
                <p class="text-danger mt-3">{{$message}}</p>
              @enderror
            </div>

            <button class="mt-1 btn cafe-search">Update Category</button>
        </form>
    </div>
</div>

@endsection
