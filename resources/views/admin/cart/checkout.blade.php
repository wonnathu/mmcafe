@extends('admin.layout.master')

@section('title', "Dashboard")

@section('content')


  <h1 class="cafe-title">Checkout</h1>
  <a href="{{route('clear')}}" class="btn cafe-search">Clear Cart</a>
  <div class="row mt-5">
    <div class="col-md-8">
      <table class="mb-0 table table-bordered">
        <thead>
          <tr class="cafe-category-table-header">
              <th>Name</th>
              <th>qnt</th>
              <th>Price</th>
              <th>Remark</th>
              <th>Action</th>
          </tr>
        </thead>
      <tbody>
        @foreach($cartItems as $item)
        <tr>
            <td>{{$item->name}}</td>
            <td>
              <form action="{{route('cart.update', $item->id)}}" method="post" class="form-inline">
                @csrf
                @method('patch')
                <div class="position-relative form-group">
                  <input type="number" value="{{$item->quantity}}" name="qnt" min="1" class="form-control mr-2">
                </div>
                <button type="submit" class="btn ">
                  <i class="metismenu-icon pe-7s-note2 h5 text-primary"></i>
                </button>
              </form>
            </td>
            <td>{{$item->quantity * $item->price}}</td>
            <td>
              <form action="{{route('cart.update', $item->id)}}" method="post" class="form-inline">
                @csrf
                @method('patch')
                <div class="position-relative form-group">
                  <input type="text" value="{{$item->attributes->remark ? $item->attributes->remark : '' }}" name="remark" class="form-control mr-2">
                </div>
                <button type="submit" class="btn ">
                  <i class="metismenu-icon pe-7s-note2 h5 text-primary"></i>
                </button>
              </form>
            </td>
            <td>
              <form action="{{route('cart.destroy', $item->id)}}" method="post" onsubmit="return confirm('{{ 'Are You Sure' }}');">
                @csrf
                @method('delete')
                <button type="submit" class="btn ">
                <i class="metismenu-icon pe-7s-trash h5 text-danger" ></i>
                </button>
              </form>
            </td>
        </tr>
        @endforeach
        <tr class=" h4 cafe-primary-color">
          <td colspan='2'>Total</td>
          <td colspan="2">{{Cart::session(auth()->id())->getTotal()}}</td>
        </tr>
      </tbody>
    </table>
    </div>
    <div class="col-md-4 widget-content p-0">
      <div class="cafe-category-table-header p-3">
        <form action="{{route('cart.order')}}" method="post" >
        @csrf
          <div class="form-group">
            <label for="table" class="text-light">Select Table</label>
            <select class="form-control" id="table" name="table_id">
              @foreach($tables as $table)
                <option value="{{$table->id}}" >{{$table->table_name}}</option>
              @endforeach
            </select>
          </div>
          <div class="mt-4">
            <button type="submit" class="btn btn-light btn-lg w-100">Order Now</button>
          </div>
        </form>
      </div>
    </div>
  </div>





@endsection
