@extends('admin.layout.master')

@section('title', "Dashboard")

@section('content')

  <h1 class="h2 text-primary" >User Setting</h1>

  <table class="mb-0 table table-hover ">
      <thead>
      <tr>
          <th colspan="2">Your Information</th>
      </tr>
      </thead>
      <tbody>
        <tr>
            <th scope="row">Name</th>
            <td>{{$user->name}}</td>
        </tr>
        <tr>
            <th scope="row">Email</th>
            <td>{{$user->email}}</td>
        </tr>

      </tbody>
    </table>


@endsection
