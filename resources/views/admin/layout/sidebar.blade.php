<div class="app-sidebar sidebar-shadow bg-happy-green sidebar-text-light">
    <div class="app-header__logo">
        <div class="logo-src"></div>
        <div class="header__pane ml-auto">
            <div>
                <button type="button" class="hamburger close-sidebar-btn hamburger--elastic" data-class="closed-sidebar">
                    <span class="hamburger-box">
                        <span class="hamburger-inner"></span>
                    </span>
                </button>
            </div>
        </div>
    </div>
    <div class="app-header__mobile-menu">
        <div>
            <button type="button" class="hamburger hamburger--elastic mobile-toggle-nav">
                <span class="hamburger-box">
                    <span class="hamburger-inner"></span>
                </span>
            </button>
        </div>
    </div>
    <div class="app-header__menu">
        <span>
            <button type="button" class="btn-icon btn-icon-only btn btn-primary btn-sm mobile-toggle-header-nav">
                <span class="btn-icon-wrapper">
                    <i class="fa fa-ellipsis-v fa-w-6"></i>
                </span>
            </button>
        </span>
    </div>
    <div class="scrollbar-sidebar">
        <div class="app-sidebar__inner">
            <ul class="vertical-nav-menu">
                <li class="app-sidebar__heading">Dashboards</li>
                <li>
                    <a href="{{route('admin')}}" class="{{Request::path() === 'admin' ? 'mm-active' : '' }}">
                        <i class="metismenu-icon pe-7s-airplay"></i>
                        Dashboard
                    </a>
                </li>
                @if(Auth::user()->role == 1)
                <li class="app-sidebar__heading">Tables</li>
                <li>
                    <a href="{{route('table.index')}}" class="{{Request::path() === 'table' ? 'mm-active' : '' }}">
                        <i class="metismenu-icon pe-7s-network"></i>
                        Tables List
                    </a>
                </li>
                <li>
                    <a href="{{route('table.create')}}" class="{{Request::path() === 'table/create' ? 'mm-active' : '' }}">
                        <i class="metismenu-icon pe-7s-pen"></i>
                        Add new Table
                    </a>
                </li>
                <li class="app-sidebar__heading">Categories</li>
                <li>
                    <a href="{{route('category.index')}}" class="{{Request::path() === 'category' ? 'mm-active' : '' }}">
                        <i class="metismenu-icon pe-7s-file"></i>
                        Cateogries List
                    </a>
                </li>
                <li>
                    <a href="{{route('category.create')}}" class="{{Request::path() === 'category/create' ? 'mm-active' : '' }}">
                        <i class="metismenu-icon pe-7s-pen"></i>
                        Add new Category
                    </a>
                </li>
                <li class="app-sidebar__heading">Menus</li>
                <li>
                    <a href="{{route('menu.index')}}" class="{{Request::path() === 'menu' ? 'mm-active' : '' }}">
                        <i class="metismenu-icon pe-7s-note2"></i>
                        Menus List
                    </a>
                </li>
                <li>
                    <a href="{{route('menu.create')}}" class="{{Request::path() === 'menu/create' ? 'mm-active' : '' }}">
                        <i class="metismenu-icon pe-7s-note"></i>
                        Add new Menu
                    </a>
                </li>

                <li class="app-sidebar__heading">Order</li>
                <li>
                    <a href="{{route('orderlist.index')}}" class="{{Request::path() === 'orderlist' ? 'mm-active' : '' }}">
                        <i class="metismenu-icon pe-7s-note2"></i>
                        Order List
                    </a>
                </li>
                @elseif(Auth::user()->role == 3 || Auth::user()->role == 2)
                <li class="app-sidebar__heading">Order</li>
                <li>
                    <a href="{{route('orderlist.index')}}" class="{{Request::path() === 'orderlist' ? 'mm-active' : '' }}">
                        <i class="metismenu-icon pe-7s-note2"></i>
                        Order List
                    </a>
                </li>
                @endif
            </ul>
        </div>
    </div>
</div>
