@extends('admin.layout.master')

@section('title', "Dashboard")

@section('content')

<div class="clearfix mb-4" >
  <div class="float-left">
    <h1 class="cafe-title">Order List</h1>
  </div>
  <div class="float-right">
    <form>
      <div class="form-row">
        <div class="col">
          <label for="dateofbirth" class='mr-2'>Start Date</label>
          <input type="date" name="startdate" id="dateofbirth" required>
        </div>
        <div class="col">
          <label for="dateofbirth" class='mr-2'>End Date</label>
          <input type="date" name="enddate" id="dateofbirth" required>
        </div>
        <div>
          <label for="dateofbirth" class='mt-4'></label>
          <button type="submit" class="btn cafe-search ml-3">
            <i class="fa fa-search"></i>
          </button>
          <a href="{{route('orderlist.index')}}" class="btn cafe-search">Clear Filter</a>
        </div>
      </div>
    </form>
  </div>
</div>

  <div class="card mb-5">
    <table class="mb-0 table table-striped ">
      <thead>
      <tr class="cafe-category-table-header">
          <th class="pt-4 pb-4">Table</th>
          <th>
            <div class="row">
              <div class="col-3 pt-3 pb-3">
                Menu Name
              </div>
              <div class="col-3 pt-3 pb-3">
                Qnt
              </div>
              <div class="col-3 pt-3 pb-3">
                Price
              </div>
              <div class="col-3 pt-3 pb-3">
                Remark
              </div>
            </div>
          </th>
          <th class="pt-4 pb-4">Date</th>
          <th class="pt-4 pb-4">Action</th>
      </tr>
      </thead>
      <tbody>
        @foreach($orders as $order)
          @if($order->status == 0)
          <tr>
            <td>{{$order->table_name}}</td>
            <td>
              @foreach($order->menus()->get() as $ord)
                <div class="row">
                <div class="col-3">
                    {{$ord->menu_name}}
                  </div>
                  <div class="col-3">
                    {{$ord->pivot->qnt}}
                  </div>
                  <div class="col-3">
                    {{$ord->price * $ord->pivot->qnt}}
                  </div>
                  <div class="col-3">
                    {{$ord->pivot->remark}}
                  </div>
                </div>

              @endforeach
            </td>
            <td>
              {{$order->created_at->format('d M Y , H:i a')}}
            </td>
            <td>
              @if(Auth::user()->role == 1)
              <form action="{{ route('orderlist.destroy', $order->id) }}" method="POST" onsubmit="return confirm('{{ 'Are You Sure' }}');" class="d-inline">
                @csrf
                @method('DELETE')
                <button type="submit" class="btn" title="Delete">
                <i class="metismenu-icon pe-7s-trash h5 text-danger" ></i>
                </button>
              </form>
              @endif
              @if(Auth::user()->role == 3)
              <a href="{{ route('orderlist.status', $order->id) }}" class="bg-success text-light px-3 py-2">Finish</a>
              @endif
              @if(Auth::user()->role ==2)
                <span class="px-3 py-2 text-success">Ordering...</span>
              @endif
            </td>
          </tr>
          @elseif($order->status == 1)
          <tr class="bg-warning">
            <td>{{$order->table_name}}</td>
            <td>
              @foreach($order->menus()->get() as $ord)
                <div class="row">
                <div class="col-4">
                    {{$ord->menu_name}}
                  </div>
                  <div class="col-4">
                    {{$ord->pivot->qnt}}
                  </div>
                  <div class="col-4">
                    {{$ord->price * $ord->pivot->qnt}}
                  </div>
                </div>

              @endforeach
            </td>
            <td>
              {{$order->created_at->format('d M Y , H:i a')}}
            </td>
            <td>
              @if(Auth::user()->role == 1)
              <form action="{{ route('orderlist.destroy', $order->id) }}" method="POST" onsubmit="return confirm('{{ 'Are You Sure' }}');" class="d-inline">
                @csrf
                @method('DELETE')
                <button type="submit" class="btn" title="Delete">
                <i class="metismenu-icon pe-7s-trash h5 text-danger" ></i>
                </button>
              </form>
              @endif
              @if(Auth::user()->role == 3)
              <a href="{{ route('orderlist.status', $order->id) }}" class="bg-danger text-light px-3 py-2">Undo</a>
              @endif
              @if(Auth::user()->role ==2)
                <span class="px-3 py-2 text-danger">Finish</span>
              @endif
            </td>
          </tr>
          @endif
        @endforeach
        
        <tr class="h3">
          <td></td>
          <td>
            <div class="row">
              <div class="col-2">
                Total
              </div>
              <div class="col-4 offset-4">
                {{$total}}
              </div>
            </div>
          </td>
          <td></td>
          <td></td>
        </tr>
        <tr><span class="mt-2 ml-2">{{$orders->links()}}</span></tr>
      </tbody>
    </table>
    <span class="mt-2 ml-2">{{$orders->links()}}</span>
    </div>


@endsection
