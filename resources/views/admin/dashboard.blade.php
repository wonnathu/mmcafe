@extends('admin.layout.master')

@section('title', "Dashboard")

@section('cart')
<div class="mr-4">
    <a href="{{route('checkout')}}" class="cafe-cart">
        <i class="fas fa-shopping-cart"></i>
        {{Cart::session(auth()->id())->getTotalQuantity()}}
    </a>
</div>

@endsection


@section('content')

  <div class="clearfix" >
    <div class="float-left">
      <h1 class="cafe-title">Menus List</h1>
    </div>
    <div class="float-right">
    <form class="form-inline">
      <div class="position-relative form-group">
        <label for="exampleEmail33" class="sr-only">Search</label>
        <select class="form-control mr-2" name="category_id">
          @foreach($filter_category as $category)
          <option value="{{$category->id}}">{{$category->category_name}}</option>
          @endforeach
        </select>
      </div>
      <button type="submit" class="btn cafe-search" title="Search Category"><i class="fas fa-search"></i></button>
      <a href="{{route('admin')}}" class="btn cafe-search ml-2" title="Reset Filter"><i class="fas fa-times"></i></a>
    </form>
    </div>
  </div>


  @foreach($categories as $category)
    @if(count($category->menus))
    <h2 class="h4 cafe-secondary-title text-center mt-5">{{$category->category_name}}</h2>
    <hr class="cafe-hr">
    <div class="row bg-light border pt-4 pb-4 mb-5">
      @foreach($category->menus as $menu)
        <div class="col-sm-6 col-md-4 col-xl-3 mb-3 mt-3">
          <div class="card h-100">
            <img src="{{ url('storage/'.str_replace('public/', '', $menu->image)) }}" class="card-img-top" alt="..." style="width: 100%; height: 150px;">
            <div class="card-body pb-0" style="background: none;">
              <h5 class="card-title cafe-card-title">{{$menu->menu_name}}
                <small class="text-secondary">({{$category->category_name}})</small>
              </h5>
              <p class="card-text text-secondary"><i class="fas fa-comment-dollar"></i> {{$menu->price}} MMK</p>
            </div>
            <div class="card-footer pb-0">
              <form action="{{route('cart')}}" method="post" >
                @csrf
                <div class="clearfix border-0">
                  <div class="form-group float-left">
                    <label for="qnt" class="sr-only">Quantity</label>
                    <input name="qnt" id="qnt" placeholder="0" type="number" min="1" class="form-control  @error('qnt') border-danger @enderror" value="1" style="width: 80px">
                    @error('qnt')
                    <p class="text-danger mt-3">{{$message}}</p>
                    @enderror
                  </div>
                  <input type="hidden" name="menu_id" value="{{$menu->id}}">
                  <input type="hidden" name="menu_name" value="{{$menu->menu_name}}">
                  <input type="hidden" name="price" value="{{$menu->price}}">
                  <button type="submit" class="btn float-right cafe-search">
                    <i class="fas fa-cart-plus text-light"></i>
                  </button>
                </div>
              </form>
            </div>
          </div>
        </div>
      @endforeach
    </div>
    @endif
  @endforeach
  <div class="row">

  </div>

@endsection
