@extends('admin.layout.master')

@section('title', "Dashboard")

@section('content')

  <div class="clearfix" >
      <div class="float-left">
        <h1 class="cafe-title">Table List</h1>
      </div>
      <div class="float-right">
        <a href="{{route('table.create')}}" class="mb-2 mr-2 btn cafe-search mb-4">Add New Table</a>
      </div>
  </div>

  <!-- <div class="card">
  <table class="mb-0 table table-hover ">
    <thead>
    <tr>
        <th>#</th>
        <th>Table Name</th>
        <th>Action</th>
    </tr>
    </thead>
    <tbody>
    @foreach($tables as $table)
      <tr>
          <th scope="row">{{$no++}}</th>
          <td>{{$table->table_name}}</td>
          <td>

            <a href="{{route('table.edit', $table->id)}}" title="Edit"><i class="metismenu-icon pe-7s-note2 h5 text-success"></i></a>
            <span class="ml-4"></span>

            <a href="#" title="Trash" onclick="document.getElementById('destroy-table-{{ $table->id }}').submit();">
              <i class="metismenu-icon pe-7s-trash h5 text-danger"></i>
            </a>
            <form action="{{ route('table.destroy', $table->id) }}" id="destroy-table-{{ $table->id }}" method="POST">
              @csrf
              @method('DELETE')
          </form>

          </td>
      </tr>
    @endforeach
    </tbody>
  </table>
  </div> -->

  <div class="row bg-light pt-4 pb-4 pl-2 pr-2">
  @foreach($tables as $table)
  <div class="col-md-6 col-xl-4 mb-3 mt-3">
        <div class="card ">
            <!-- <div class="widget-content-wrapper text-white">
                <div class="widget-content-left">
                    <div class="widget-heading">Table No. {{$table->table_name}}</div>
                    <div class="mt-2">
                      <a href="{{route('table.edit', $table->id)}}" title="Edit" class="btn btn-light">
                        <i class="metismenu-icon pe-7s-note2 h5 text-success"></i>
                      </a>
                      <form action="{{ route('table.destroy', $table->id) }}" method="POST" onsubmit="return confirm('{{ 'Are You Sure' }}');" class="d-inline">
                        @csrf
                        @method('DELETE')
                        <button type="submit" class="btn btn-light" title="Delete">
                        <i class="metismenu-icon pe-7s-trash h5 text-danger" ></i>
                        </button>
                      </form>
                    </div>
                </div>
                <div class="widget-content-right">
                    <div class="widget-numbers text-white"><span class="h1"> {{$table->table_name}}</span></div>
                </div>
            </div> -->
            <h1 class="text-center pt-4 pb-4 cafe-table-title"> <i class="fas fa-couch"></i> {{$table->table_name}}</h1>
            <div class="mt-2 clearfix">
              <span class='h6 text-secondary p-3 float-left'>Table NO. ({{$table->table_name}})</span>
              <div class="float-right">
                <a href="{{route('table.edit', $table->id)}}" title="Edit" class="btn">
                  <i class="metismenu-icon pe-7s-note2 h5 text-primary"></i>
                </a>
                <form action="{{ route('table.destroy', $table->id) }}" method="POST" onsubmit="return confirm('{{ 'Are You Sure' }}');" class="d-inline">
                  @csrf
                  @method('DELETE')
                  <button type="submit" class="btn" title="Delete">
                  <i class="metismenu-icon pe-7s-trash h5 text-danger" ></i>
                  </button>
                </form>
              </div>
            </div>
        </div>
    </div>
    @endforeach
  </div>

@endsection
