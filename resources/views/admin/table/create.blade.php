@extends('admin.layout.master')

@section('title', "Dashboard")

@section('content')

<div class="clearfix" >
  <div class="float-left">
    <h1 class="cafe-title">Add New Table</h1>
  </div>
  <div class="float-right">
    <a href="{{route('table.index')}}" class="mb-2 mr-2 btn cafe-search mb-4">Back</a>
  </div>
</div>



  <div class="main-card mb-3 card">
    <div class="card-body p-5">
        <form class="" method="post" action="{{route('table.store')}}">
        @csrf
            <div class="position-relative form-group">
              <label for="name" class="">Table Name</label>
              <input name="table_name" id="name" placeholder="Enter the table name" type="text" class="form-control @error('table_name') border-danger @enderror">
              @error('table_name')
                <p class="text-danger mt-3">{{$message}}</p>
              @enderror
            </div>

            <button class="mt-1 btn cafe-search">Add New</button>
        </form>
    </div>
</div>

@endsection
