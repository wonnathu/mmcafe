@extends('admin.layout.master')

@section('title', "Dashboard")

@section('content')
<div class="clearfix" >
  <div class="float-left">
    <h1 class="cafe-title">Menu List</h1>
  </div>
  <div class="float-right">
    <a href="{{route('menu.create')}}" class="mb-2 mr-2 btn cafe-search mb-4">Add New Menu</a>
  </div>
</div>

<div class="mt-5 mb-4">
      {{$menus->links()}}
</div>

  <div class="card">
    <table class="mb-0 table table-striped ">
      <thead>
      <tr class="cafe-category-table-header">
          <th class="pt-4 pb-4">#</th>
          <th class="pt-4 pb-4">Menu Name</th>
          <th class="pt-4 pb-4">Price</th>
          <th class="pt-4 pb-4">Image</th>
          <th class="pt-4 pb-4">Category</th>
          <th class="pt-4 pb-4">Action</th>
      </tr>
      </thead>
      <tbody>
      @foreach($menus as $menu)
        <tr>
            <th scope="row">{{$no++}}</th>
            <td>{{$menu->menu_name}}</td>
            <td>{{$menu->price}}</td>
            <td>
              <img src="{{ url('storage/'.str_replace('public/', '', $menu->image)) }}" class="card-img-top" alt="..." style="width: 200px; height: 150px" >
            </td>
            <td>{{$menu->category->category_name}}</td>
            <td>

                <a href="{{route('menu.edit', $menu->id)}}" title="Edit" class="btn">
                  <i class="metismenu-icon pe-7s-note2 h5 text-success"></i>
                </a>


                <form action="{{ route('menu.destroy', $menu->id) }}" onsubmit="return confirm('{{ 'Are You Sure' }}');" method="POST" class="d-inline">
                  @csrf
                  @method('DELETE')
                  <button type="submit" class="btn" title="Delete">
                    <i class="metismenu-icon pe-7s-trash h5 text-danger" ></i>
                  </button>
              </form>

              </td>
        </tr>
      @endforeach
      </tbody>
    </table>
</div>
<div class="mt-5 mb-5">
      {{$menus->links()}}
</div>


@endsection
