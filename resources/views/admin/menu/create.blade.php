@extends('admin.layout.master')

@section('title', "Dashboard")

@section('content')
<div class="clearfix" >
  <div class="float-left">
    <h1 class="cafe-title">Add New Menu</h1>
  </div>
  <div class="float-right">
    <a href="{{route('menu.index')}}" class="mb-2 mr-2 btn cafe-search mb-4">Back</a>
  </div>
</div>

  <div class="main-card mb-3 card">
    <div class="card-body p-5">
        <form class="" method="post" action="{{route('menu.store')}}" enctype='multipart/form-data'>
        @csrf
            <div class="position-relative form-group">
              <label for="name" class="">Menu Name</label>
              <input name="menu_name" id="name" placeholder="Enter the menu name" type="text" class="form-control @error('menu_name') border-danger @enderror">
              @error('menu_name')
                <p class="text-danger mt-3">{{$message}}</p>
              @enderror
            </div>

            <div class="position-relative form-group">
              <label for="price" class="">Price</label>
              <input name="price" id="price" placeholder="Enter the price" type="number" class="form-control @error('price') border-danger @enderror">
              @error('price')
                <p class="text-danger mt-3">{{$message}}</p>
              @enderror
            </div>

            <div class="position-relative form-group">
              <label for="image" class="">Menu Image</label>
              <input name="image" id="image" type="file" class="form-control-file">
            </div>

            <div class="position-relative form-group">
              <select class="mb-2 form-control" name="category_id">
              @foreach($categories as $category)
                  <option value="{{$category->id}}">{{$category->category_name}}</option>
              @endforeach
              </select>
            </div>

            <button class="mt-1 btn cafe-search">Add New</button>
        </form>
    </div>
</div>

@endsection
